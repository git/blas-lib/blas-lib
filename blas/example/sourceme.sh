export COMPILER_BASE_DIR="/opt/ti/TI_CGT_C6000_7.4.2"
export C6X_C_DIR="${COMPILER_BASE_DIR}/include;${COMPILER_BASE_DIR}/lib"
export PATH="${COMPILER_BASE_DIR}/bin:$PATH"
export XDCROOT="/opt/ti/xdctools_3_25_02_70/"

# Set up envs for the ECPY example
TI_BASE_DIR="/opt/ti"
export EDMA3LLD_DIR=$TI_BASE_DIR/edma3_lld_02_11_10_09/packages
export BIOS_DIR=$TI_BASE_DIR/bios_6_35_04_50/packages
export XDC_DIR=$TI_BASE_DIR/xdctools_3_25_02_70
export CGEN_DIR=$COMPILER_BASE_DIR
export FC_DIR=$TI_BASE_DIR/framework_components_3_30_00_04_eng/packages
export XDAIS_DIR=$TI_BASE_DIR/xdais_7_23_00_06/packages
