/******************************************************************************
 * FILE NAME:   main.c
 *
 * (C) Copyright 2013, Texas Instruments Incorporated.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <xdc/std.h>
#include <string.h>
#include <stdio.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/family/c66/Cache.h>
#include <ti/xdais/ires.h>

#include "../../alg/algLoc.h"
#include "../../alg/algEdma.h"
#include "./rman/appRman.h"

int main_C6678(int argc, char **args); /* Expect user main redefined to main_C6678. */

extern cregister volatile unsigned int DNUM;
static algInst_t algInstance;                   /* Static to only appear here. */

/*-----------------------------------------------------------------------*/
/* alg_create returns address of algInstance defined above.              */
/*-----------------------------------------------------------------------*/
void alg_create(void **pAlgHandle)
{
    *pAlgHandle = (void *)&algInstance;
}

/*********************************************************************************
 * FUNCTION PURPOSE: trap exception
 *********************************************************************************
  DESCRIPTION:      This function traps exception
  Parameters :      Inputs: statement   : statement for if check
                            core_id     : core ID
                            error       : error to be printed out
********************************************************************************/
void app_assert(int32_t statement, int32_t core_id, const char *error)
{
  volatile int32_t dbg_halt = 1;

  if(!statement) {
    printf("%s (%d)\n",error,core_id);
    while(dbg_halt);
  }
}

/* Base and size of DDR3 */
#define EXT_MEM_BASE (0x80000000)
#define EXT_MEM_SIZE (0x20000000)
/*********************************************************************************
 * FUNCTION PURPOSE: configure cache
 *********************************************************************************
  DESCRIPTION:      This function configure cache, including L1D cache size
                    L1P cache size, L2 cache size, and DDR MAR settings
  Note: The sysbios apparently needs some things in L2, about 34K worth. We 
        can't afford to lose that in ATLAS, so we define L2Size as 128K (in the
        development environment we had 256K), so that leaves 384K. We leave the
        first 64K to sysbios; and internally start our area at 64K. We only 
        expect to use < 256K, so no more than 320K (of the 384K available).
        Tony C.
********************************************************************************/
void cache_config(void)
{

    Cache_Size size;
    UInt32 mar;

    size.l1pSize  = Cache_L1Size_32K;  /* L1P cache size */
    size.l1dSize  = Cache_L1Size_4K;   /* L1D cache size */
    size.l2Size   = Cache_L2Size_128K; /* L2  cache size */
    Cache_setSize(&size);

    mar = Cache_getMar((Ptr *)EXT_MEM_BASE) | ti_sysbios_family_c66_Cache_PC |
                                              ti_sysbios_family_c66_Cache_PFX;
    Cache_setMar((Ptr *)EXT_MEM_BASE, EXT_MEM_SIZE, mar);

    Cache_wbInvAll();
}


/*********************************************************************************
 * FUNCTION PURPOSE: main()
********************************************************************************/
int main(int argc, char **args)
{
   uint32_t edmaInstances[8] = {1, 1, 1, 1, 2, 2, 2, 2};
   int32_t  core_id = DNUM;
   int      ret_val;
   int i;
   void *algHandle;
   algInst_t *inst;

   /* Cache configuration */
   cache_config();

   /* Create Alg */
   alg_create(&algHandle);

   app_assert( (EdmaMgr_init(DNUM, NULL) == EdmaMgr_SUCCESS), DNUM, "EdmaMgr_init() return error!");
   inst = (algInst_t*)algHandle;
   inst->alg_edma_status = ALG_EDMA_STATE_INIT;

   /* RMAN assign resource to Alg*/
   ret_val = app_rman_assign_resources(algHandle, NULL);
   app_assert( (ret_val == TRUE), core_id, "assign_resources failed \n");

   i = main_C6678(argc, args);                  // Transfer to new main.

   /* RMAN free resource*/
   ret_val = app_rman_free_resources(algHandle, NULL);
   app_assert( (ret_val == TRUE), core_id, "free_resources failed \n");
   return(i);                                   // Exit with main_C6678 ret value.
}

