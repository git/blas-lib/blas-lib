/******************************************************************************
 * FILE PURPOSE: Using FC-RMAN
 ******************************************************************************
 * FILE NAME:   appRman.c
 *
 * (C) Copyright 2013, Texas Instruments Incorporated.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#include <xdc/std.h>
#include <ti/xdais/ires.h>

/* Get globals from cfg header */
#include <xdc/cfg/global.h>

#include <algLoc.h>
#include <algEdma.h>
#include <appRman.h>

//#define FC_TRACE
#ifdef FC_TRACE
#include <ti/sdo/fc/global/FCSettings.h>
#include <xdc/runtime/Diags.h>
#endif

/*********************************************************************************
 * FUNCTION PURPOSE: Assign and activate EdmaMgr resources
 *********************************************************************************
  DESCRIPTION:      This function assigns and activates RMAN resouces

  Parameters :      Inputs: algHandle   : alg handle
                            resFxns     : IRES function pointers
                    Output: TRUE if initialization successful; FALSE otherwise
 *********************************************************************************/
Bool app_rman_assign_resources(void* algHandle, void* resFxns)
{
  algInst_t *inst;
  ALG_EDMA_Struct *algEdmaState;

  inst = (algInst_t*)algHandle;
  algEdmaState = &(inst->alg_edma_state); 
  if ( inst->alg_edma_status != ALG_EDMA_STATE_ALLOCATED )
  {
    algEdmaState->num_channels = 0;

    while ( algEdmaState->num_channels < ALG_NUM_EDMA_CH )
    {
      algEdmaState->channel[algEdmaState->num_channels] = EdmaMgr_alloc(ALG_MAX_EDMA_LINKS);
    
      if (algEdmaState->channel[algEdmaState->num_channels] == NULL)
      	return FALSE;

      algEdmaState->num_channels++;
    }
  }
  inst->alg_edma_status = ALG_EDMA_STATE_ALLOCATED;

  return TRUE;
}

/*********************************************************************************
 * FUNCTION PURPOSE: Free EdmaMgr resources
 *********************************************************************************
  DESCRIPTION:      This function frees EdmaMgr resouces

  Parameters :      Inputs: algHandle   : alg handle
                            resFxns     : IRES function pointers
                    Output: TRUE if free is successful; FALSE otherwise
 *********************************************************************************/
Bool app_rman_free_resources(void* algHandle, void* resFxns)
{
  int      ret_val;
  algInst_t *inst;
  ALG_EDMA_Struct *algEdmaState;

  inst = (algInst_t*)algHandle;
  algEdmaState = &(inst->alg_edma_state); 

  /* RMAN free resource */
  if ( inst->alg_edma_status == ALG_EDMA_STATE_ALLOCATED )
  {
    while ( algEdmaState->num_channels > 0 )
    {
      ret_val = EdmaMgr_free(algEdmaState->channel[algEdmaState->num_channels-1]);
      if (ret_val != EdmaMgr_SUCCESS)
      	return FALSE;
    
      algEdmaState->num_channels--;
    }
  }
  inst->alg_edma_status = ALG_EDMA_STATE_INIT;

  return TRUE;
}

