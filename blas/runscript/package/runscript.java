/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-x14
 */
import java.util.*;
import org.mozilla.javascript.*;
import xdc.services.intern.xsr.*;
import xdc.services.spec.Session;

public class runscript
{
    static final String VERS = "@(#) xdc-x14\n";

    static final Proto.Elm $$T_Bool = Proto.Elm.newBool();
    static final Proto.Elm $$T_Num = Proto.Elm.newNum();
    static final Proto.Elm $$T_Str = Proto.Elm.newStr();
    static final Proto.Elm $$T_Obj = Proto.Elm.newObj();

    static final Proto.Fxn $$T_Met = new Proto.Fxn(null, null, 0, -1, false);
    static final Proto.Map $$T_Map = new Proto.Map($$T_Obj);
    static final Proto.Arr $$T_Vec = new Proto.Arr($$T_Obj);

    static final XScriptO $$DEFAULT = Value.DEFAULT;
    static final Object $$UNDEF = Undefined.instance;

    static final Proto.Obj $$Package = (Proto.Obj)Global.get("$$Package");
    static final Proto.Obj $$Module = (Proto.Obj)Global.get("$$Module");
    static final Proto.Obj $$Instance = (Proto.Obj)Global.get("$$Instance");
    static final Proto.Obj $$Params = (Proto.Obj)Global.get("$$Params");

    static final Object $$objFldGet = Global.get("$$objFldGet");
    static final Object $$objFldSet = Global.get("$$objFldSet");
    static final Object $$proxyGet = Global.get("$$proxyGet");
    static final Object $$proxySet = Global.get("$$proxySet");
    static final Object $$delegGet = Global.get("$$delegGet");
    static final Object $$delegSet = Global.get("$$delegSet");

    Scriptable xdcO;
    Session ses;
    Value.Obj om;

    boolean isROV;
    boolean isCFG;

    Proto.Obj pkgP;
    Value.Obj pkgV;

    ArrayList<Object> imports = new ArrayList<Object>();
    ArrayList<Object> loggables = new ArrayList<Object>();
    ArrayList<Object> mcfgs = new ArrayList<Object>();
    ArrayList<Object> proxies = new ArrayList<Object>();
    ArrayList<Object> sizes = new ArrayList<Object>();
    ArrayList<Object> tdefs = new ArrayList<Object>();

    void $$IMPORTS()
    {
        Global.callFxn("loadPackage", xdcO, "xdc");
        Global.callFxn("loadPackage", xdcO, "xdc.corevers");
    }

    void $$OBJECTS()
    {
        pkgP = (Proto.Obj)om.bind("runscript.Package", new Proto.Obj());
        pkgV = (Value.Obj)om.bind("runscript", new Value.Obj("runscript", pkgP));
    }

    void Main$$OBJECTS()
    {
        Proto.Obj po, spo;
        Value.Obj vo;

        po = (Proto.Obj)om.bind("runscript.Main.Module", new Proto.Obj());
        vo = (Value.Obj)om.bind("runscript.Main", new Value.Obj("runscript.Main", po));
        pkgV.bind("Main", vo);
        // decls 
        spo = (Proto.Obj)om.bind("runscript.Main$$RegressionCpu", new Proto.Obj());
        om.bind("runscript.Main.RegressionCpu", new Proto.Str(spo, true));
        spo = (Proto.Obj)om.bind("runscript.Main$$PlatformTargetMap", new Proto.Obj());
        om.bind("runscript.Main.PlatformTargetMap", new Proto.Str(spo, true));
    }

    void Run$$OBJECTS()
    {
        Proto.Obj po, spo;
        Value.Obj vo;

        po = (Proto.Obj)om.bind("runscript.Run.Module", new Proto.Obj());
        vo = (Value.Obj)om.bind("runscript.Run", new Value.Obj("runscript.Run", po));
        pkgV.bind("Run", vo);
        // decls 
    }

    void Main$$CONSTS()
    {
        // module Main
    }

    void Run$$CONSTS()
    {
        // module Run
    }

    void Main$$CREATES()
    {
        Proto.Fxn fxn;
        StringBuilder sb;

    }

    void Run$$CREATES()
    {
        Proto.Fxn fxn;
        StringBuilder sb;

    }

    void Main$$FUNCTIONS()
    {
        Proto.Fxn fxn;

        // fxn Main.run
        fxn = (Proto.Fxn)om.bind("runscript.Main$$run", new Proto.Fxn(om.findStrict("runscript.Main.Module", "runscript"), null, 1, 1, false));
                fxn.addArg(0, "args", $$T_Obj, $$UNDEF);
        // fxn Main.addCpu
        fxn = (Proto.Fxn)om.bind("runscript.Main$$addCpu", new Proto.Fxn(om.findStrict("runscript.Main.Module", "runscript"), null, 3, 3, false));
                fxn.addArg(0, "buildCpuName", $$T_Str, $$UNDEF);
                fxn.addArg(1, "ccsName", $$T_Str, $$UNDEF);
                fxn.addArg(2, "alwaysConnect", $$T_Bool, $$UNDEF);
        // fxn Main.getFreeCpu
        fxn = (Proto.Fxn)om.bind("runscript.Main$$getFreeCpu", new Proto.Fxn(om.findStrict("runscript.Main.Module", "runscript"), (Proto)om.findStrict("runscript.Main.RegressionCpu", "runscript"), 1, 1, false));
                fxn.addArg(0, "buildCpuName", $$T_Str, $$UNDEF);
        // fxn Main.getCpu
        fxn = (Proto.Fxn)om.bind("runscript.Main$$getCpu", new Proto.Fxn(om.findStrict("runscript.Main.Module", "runscript"), (Proto)om.findStrict("runscript.Main.RegressionCpu", "runscript"), 2, 2, false));
                fxn.addArg(0, "buildCpuName", $$T_Str, $$UNDEF);
                fxn.addArg(1, "cpuNum", Proto.Elm.newCNum("(xdc_UInt)"), $$UNDEF);
        // fxn Main.containsCpu
        fxn = (Proto.Fxn)om.bind("runscript.Main$$containsCpu", new Proto.Fxn(om.findStrict("runscript.Main.Module", "runscript"), $$T_Bool, 1, 1, false));
                fxn.addArg(0, "buildCpuName", $$T_Str, $$UNDEF);
        // fxn Main.setPlatformTarget
        fxn = (Proto.Fxn)om.bind("runscript.Main$$setPlatformTarget", new Proto.Fxn(om.findStrict("runscript.Main.Module", "runscript"), null, 2, 2, false));
                fxn.addArg(0, "platform", $$T_Str, $$UNDEF);
                fxn.addArg(1, "target", $$T_Str, $$UNDEF);
        // fxn Main.getPlatformTarget
        fxn = (Proto.Fxn)om.bind("runscript.Main$$getPlatformTarget", new Proto.Fxn(om.findStrict("runscript.Main.Module", "runscript"), $$T_Str, 1, 1, false));
                fxn.addArg(0, "platform", $$T_Str, $$UNDEF);
        // fxn Main.getSession
        fxn = (Proto.Fxn)om.bind("runscript.Main$$getSession", new Proto.Fxn(om.findStrict("runscript.Main.Module", "runscript"), $$T_Obj, 1, 1, false));
                fxn.addArg(0, "coreNum", Proto.Elm.newCNum("(xdc_Int)"), $$UNDEF);
        // fxn Main.connectCpus
        fxn = (Proto.Fxn)om.bind("runscript.Main$$connectCpus", new Proto.Fxn(om.findStrict("runscript.Main.Module", "runscript"), null, 1, 1, false));
                fxn.addArg(0, "debugServer", $$T_Obj, $$UNDEF);
        // fxn Main.terminateSessions
        fxn = (Proto.Fxn)om.bind("runscript.Main$$terminateSessions", new Proto.Fxn(om.findStrict("runscript.Main.Module", "runscript"), null, 0, 0, false));
        // fxn Main.resetCpus
        fxn = (Proto.Fxn)om.bind("runscript.Main$$resetCpus", new Proto.Fxn(om.findStrict("runscript.Main.Module", "runscript"), null, 0, 0, false));
        // fxn Main.setUsed
        fxn = (Proto.Fxn)om.bind("runscript.Main$$setUsed", new Proto.Fxn(om.findStrict("runscript.Main.Module", "runscript"), null, 1, 1, false));
                fxn.addArg(0, "ccsCpuName", $$T_Str, $$UNDEF);
        // fxn Main.setUnusedCpus
        fxn = (Proto.Fxn)om.bind("runscript.Main$$setUnusedCpus", new Proto.Fxn(om.findStrict("runscript.Main.Module", "runscript"), null, 0, 0, false));
        // fxn Main.logPrint
        fxn = (Proto.Fxn)om.bind("runscript.Main$$logPrint", new Proto.Fxn(om.findStrict("runscript.Main.Module", "runscript"), null, 1, 1, false));
                fxn.addArg(0, "str", $$T_Str, $$UNDEF);
    }

    void Run$$FUNCTIONS()
    {
        Proto.Fxn fxn;

        // fxn Run.runTest
        fxn = (Proto.Fxn)om.bind("runscript.Run$$runTest", new Proto.Fxn(om.findStrict("runscript.Run.Module", "runscript"), null, 2, 2, false));
                fxn.addArg(0, "fileName", $$T_Str, $$UNDEF);
                fxn.addArg(1, "numCores", Proto.Elm.newCNum("(xdc_Int)"), $$UNDEF);
    }

    void Main$$SIZES()
    {
    }

    void Run$$SIZES()
    {
    }

    void Main$$TYPES()
    {
        Scriptable cap;
        Proto.Obj po;
        Proto.Str ps;
        Proto.Typedef pt;
        Object fxn;

        cap = (Scriptable)Global.callFxn("loadCapsule", xdcO, "runscript/Main.xs");
        om.bind("runscript.Main$$capsule", cap);
        po = (Proto.Obj)om.findStrict("runscript.Main.Module", "runscript");
        po.init("runscript.Main.Module", $$Module);
                po.addFld("$hostonly", $$T_Num, 1, "r");
        po.addFld("testListFileName", $$T_Str, "", "wh");
        po.addFld("CCSPlatform", $$T_Str, "", "wh");
        po.addFld("shortPlatformName", $$T_Str, "", "wh");
        po.addFld("cpus", new Proto.Arr((Proto)om.findStrict("runscript.Main.RegressionCpu", "runscript"), true), Global.newArray(new Object[]{}), "wh");
        po.addFld("platformTargetMap", new Proto.Arr((Proto)om.findStrict("runscript.Main.PlatformTargetMap", "runscript"), true), Global.newArray(new Object[]{}), "wh");
        po.addFld("ccxmlConfigFile", $$T_Str, "", "wh");
        po.addFld("profile", $$T_Str, "", "wh");
        po.addFld("readTestList", $$T_Bool, false, "wh");
        po.addFld("writeTestListOnly", $$T_Bool, false, "wh");
        po.addFld("settings", $$T_Obj, $$UNDEF, "wh");
        po.addFld("numCores", Proto.Elm.newCNum("(xdc_Int)"), $$UNDEF, "wh");
        po.addFld("fileName", $$T_Str, $$UNDEF, "wh");
        po.addFld("enableTracing", $$T_Bool, false, "wh");
        fxn = Global.get(cap, "module$use");
        if (fxn != null) om.bind("runscript.Main$$module$use", true);
        if (fxn != null) po.addFxn("module$use", $$T_Met, fxn);
        fxn = Global.get(cap, "module$meta$init");
        if (fxn != null) om.bind("runscript.Main$$module$meta$init", true);
        if (fxn != null) po.addFxn("module$meta$init", $$T_Met, fxn);
        fxn = Global.get(cap, "module$validate");
        if (fxn != null) om.bind("runscript.Main$$module$validate", true);
        if (fxn != null) po.addFxn("module$validate", $$T_Met, fxn);
                po.addFxn("run", (Proto.Fxn)om.findStrict("runscript.Main$$run", "runscript"), Global.get(cap, "run"));
                po.addFxn("addCpu", (Proto.Fxn)om.findStrict("runscript.Main$$addCpu", "runscript"), Global.get(cap, "addCpu"));
                po.addFxn("getFreeCpu", (Proto.Fxn)om.findStrict("runscript.Main$$getFreeCpu", "runscript"), Global.get(cap, "getFreeCpu"));
                po.addFxn("getCpu", (Proto.Fxn)om.findStrict("runscript.Main$$getCpu", "runscript"), Global.get(cap, "getCpu"));
                po.addFxn("containsCpu", (Proto.Fxn)om.findStrict("runscript.Main$$containsCpu", "runscript"), Global.get(cap, "containsCpu"));
                po.addFxn("setPlatformTarget", (Proto.Fxn)om.findStrict("runscript.Main$$setPlatformTarget", "runscript"), Global.get(cap, "setPlatformTarget"));
                po.addFxn("getPlatformTarget", (Proto.Fxn)om.findStrict("runscript.Main$$getPlatformTarget", "runscript"), Global.get(cap, "getPlatformTarget"));
                po.addFxn("getSession", (Proto.Fxn)om.findStrict("runscript.Main$$getSession", "runscript"), Global.get(cap, "getSession"));
                po.addFxn("connectCpus", (Proto.Fxn)om.findStrict("runscript.Main$$connectCpus", "runscript"), Global.get(cap, "connectCpus"));
                po.addFxn("terminateSessions", (Proto.Fxn)om.findStrict("runscript.Main$$terminateSessions", "runscript"), Global.get(cap, "terminateSessions"));
                po.addFxn("resetCpus", (Proto.Fxn)om.findStrict("runscript.Main$$resetCpus", "runscript"), Global.get(cap, "resetCpus"));
                po.addFxn("setUsed", (Proto.Fxn)om.findStrict("runscript.Main$$setUsed", "runscript"), Global.get(cap, "setUsed"));
                po.addFxn("setUnusedCpus", (Proto.Fxn)om.findStrict("runscript.Main$$setUnusedCpus", "runscript"), Global.get(cap, "setUnusedCpus"));
                po.addFxn("logPrint", (Proto.Fxn)om.findStrict("runscript.Main$$logPrint", "runscript"), Global.get(cap, "logPrint"));
        // struct Main.RegressionCpu
        po = (Proto.Obj)om.findStrict("runscript.Main$$RegressionCpu", "runscript");
        po.init("runscript.Main.RegressionCpu", null);
                po.addFld("$hostonly", $$T_Num, 1, "r");
                po.addFld("buildName", $$T_Str, $$UNDEF, "w");
                po.addFld("ccsName", $$T_Str, $$UNDEF, "w");
                po.addFld("alwaysConnect", $$T_Bool, $$UNDEF, "w");
                po.addFld("session", $$T_Obj, $$UNDEF, "w");
        // struct Main.PlatformTargetMap
        po = (Proto.Obj)om.findStrict("runscript.Main$$PlatformTargetMap", "runscript");
        po.init("runscript.Main.PlatformTargetMap", null);
                po.addFld("$hostonly", $$T_Num, 1, "r");
                po.addFld("platform", $$T_Str, $$UNDEF, "w");
                po.addFld("target", $$T_Str, $$UNDEF, "w");
    }

    void Run$$TYPES()
    {
        Scriptable cap;
        Proto.Obj po;
        Proto.Str ps;
        Proto.Typedef pt;
        Object fxn;

        cap = (Scriptable)Global.callFxn("loadCapsule", xdcO, "runscript/Run.xs");
        om.bind("runscript.Run$$capsule", cap);
        po = (Proto.Obj)om.findStrict("runscript.Run.Module", "runscript");
        po.init("runscript.Run.Module", $$Module);
                po.addFld("$hostonly", $$T_Num, 1, "r");
        po.addFld("tempLogDir", $$T_Str, "", "wh");
        fxn = Global.get(cap, "module$use");
        if (fxn != null) om.bind("runscript.Run$$module$use", true);
        if (fxn != null) po.addFxn("module$use", $$T_Met, fxn);
        fxn = Global.get(cap, "module$meta$init");
        if (fxn != null) om.bind("runscript.Run$$module$meta$init", true);
        if (fxn != null) po.addFxn("module$meta$init", $$T_Met, fxn);
        fxn = Global.get(cap, "module$validate");
        if (fxn != null) om.bind("runscript.Run$$module$validate", true);
        if (fxn != null) po.addFxn("module$validate", $$T_Met, fxn);
                po.addFxn("runTest", (Proto.Fxn)om.findStrict("runscript.Run$$runTest", "runscript"), Global.get(cap, "runTest"));
    }

    void Main$$ROV()
    {
    }

    void Run$$ROV()
    {
    }

    void $$SINGLETONS()
    {
        pkgP.init("runscript.Package", (Proto.Obj)om.findStrict("xdc.IPackage.Module", "runscript"));
        pkgP.bind("$capsule", $$UNDEF);
        pkgV.init2(pkgP, "runscript", Value.DEFAULT, false);
        pkgV.bind("$name", "runscript");
        pkgV.bind("$category", "Package");
        pkgV.bind("$$qn", "runscript.");
        pkgV.bind("$vers", Global.newArray());
        Value.Map atmap = (Value.Map)pkgV.getv("$attr");
        atmap.seal("length");
        imports.clear();
        pkgV.bind("$imports", imports);
        StringBuilder sb = new StringBuilder();
        sb.append("var pkg = xdc.om['runscript'];\n");
        sb.append("if (pkg.$vers.length >= 3) {\n");
            sb.append("pkg.$vers.push(Packages.xdc.services.global.Vers.getDate(xdc.csd() + '/..'));\n");
        sb.append("}\n");
        sb.append("pkg.build.libraries = [\n");
        sb.append("];\n");
        sb.append("pkg.build.libDesc = [\n");
        sb.append("];\n");
        Global.eval(sb.toString());
    }

    void Main$$SINGLETONS()
    {
        Proto.Obj po;
        Value.Obj vo;

        vo = (Value.Obj)om.findStrict("runscript.Main", "runscript");
        po = (Proto.Obj)om.findStrict("runscript.Main.Module", "runscript");
        vo.init2(po, "runscript.Main", $$DEFAULT, false);
        vo.bind("Module", po);
        vo.bind("$category", "Module");
        vo.bind("$capsule", om.findStrict("runscript.Main$$capsule", "runscript"));
        vo.bind("$package", om.findStrict("runscript", "runscript"));
        tdefs.clear();
        proxies.clear();
        mcfgs.clear();
        vo.bind("RegressionCpu", om.findStrict("runscript.Main.RegressionCpu", "runscript"));
        tdefs.add(om.findStrict("runscript.Main.RegressionCpu", "runscript"));
        vo.bind("PlatformTargetMap", om.findStrict("runscript.Main.PlatformTargetMap", "runscript"));
        tdefs.add(om.findStrict("runscript.Main.PlatformTargetMap", "runscript"));
        vo.bind("$$tdefs", Global.newArray(tdefs.toArray()));
        vo.bind("$$proxies", Global.newArray(proxies.toArray()));
        vo.bind("$$mcfgs", Global.newArray(mcfgs.toArray()));
        ((Value.Arr)pkgV.getv("$modules")).add(vo);
        ((Value.Arr)om.findStrict("$modules", "runscript")).add(vo);
        vo.bind("$$instflag", 0);
        vo.bind("$$iobjflag", 1);
        vo.bind("$$sizeflag", 1);
        vo.bind("$$dlgflag", 0);
        vo.bind("$$iflag", 0);
        vo.bind("$$romcfgs", "|");
        Proto.Str ps = (Proto.Str)vo.find("Module_State");
        if (ps != null) vo.bind("$object", ps.newInstance());
        vo.bind("$$meta_iobj", om.has("runscript.Main$$instance$static$init", null) ? 1 : 0);
        vo.bind("$$fxntab", Global.newArray());
        vo.bind("$$logEvtCfgs", Global.newArray());
        vo.bind("$$errorDescCfgs", Global.newArray());
        vo.bind("$$assertDescCfgs", Global.newArray());
        Value.Map atmap = (Value.Map)vo.getv("$attr");
        atmap.seal("length");
        pkgV.bind("Main", vo);
        ((Value.Arr)pkgV.getv("$unitNames")).add("Main");
    }

    void Run$$SINGLETONS()
    {
        Proto.Obj po;
        Value.Obj vo;

        vo = (Value.Obj)om.findStrict("runscript.Run", "runscript");
        po = (Proto.Obj)om.findStrict("runscript.Run.Module", "runscript");
        vo.init2(po, "runscript.Run", $$DEFAULT, false);
        vo.bind("Module", po);
        vo.bind("$category", "Module");
        vo.bind("$capsule", om.findStrict("runscript.Run$$capsule", "runscript"));
        vo.bind("$package", om.findStrict("runscript", "runscript"));
        tdefs.clear();
        proxies.clear();
        mcfgs.clear();
        vo.bind("$$tdefs", Global.newArray(tdefs.toArray()));
        vo.bind("$$proxies", Global.newArray(proxies.toArray()));
        vo.bind("$$mcfgs", Global.newArray(mcfgs.toArray()));
        ((Value.Arr)pkgV.getv("$modules")).add(vo);
        ((Value.Arr)om.findStrict("$modules", "runscript")).add(vo);
        vo.bind("$$instflag", 0);
        vo.bind("$$iobjflag", 1);
        vo.bind("$$sizeflag", 1);
        vo.bind("$$dlgflag", 0);
        vo.bind("$$iflag", 0);
        vo.bind("$$romcfgs", "|");
        Proto.Str ps = (Proto.Str)vo.find("Module_State");
        if (ps != null) vo.bind("$object", ps.newInstance());
        vo.bind("$$meta_iobj", om.has("runscript.Run$$instance$static$init", null) ? 1 : 0);
        vo.bind("$$fxntab", Global.newArray());
        vo.bind("$$logEvtCfgs", Global.newArray());
        vo.bind("$$errorDescCfgs", Global.newArray());
        vo.bind("$$assertDescCfgs", Global.newArray());
        Value.Map atmap = (Value.Map)vo.getv("$attr");
        atmap.seal("length");
        pkgV.bind("Run", vo);
        ((Value.Arr)pkgV.getv("$unitNames")).add("Run");
    }

    void $$INITIALIZATION()
    {
        Value.Obj vo;

        if (isCFG) {
        }//isCFG
        Global.callFxn("module$meta$init", (Scriptable)om.findStrict("runscript.Main", "runscript"));
        Global.callFxn("module$meta$init", (Scriptable)om.findStrict("runscript.Run", "runscript"));
        Global.callFxn("init", pkgV);
        ((Value.Obj)om.getv("runscript.Main")).bless();
        ((Value.Obj)om.getv("runscript.Run")).bless();
        ((Value.Arr)om.findStrict("$packages", "runscript")).add(pkgV);
    }

    public void exec( Scriptable xdcO, Session ses )
    {
        this.xdcO = xdcO;
        this.ses = ses;
        om = (Value.Obj)xdcO.get("om", null);

        Object o = om.geto("$name");
        String s = o instanceof String ? (String)o : null;
        isCFG = s != null && s.equals("cfg");
        isROV = s != null && s.equals("rov");

        $$IMPORTS();
        $$OBJECTS();
        Main$$OBJECTS();
        Run$$OBJECTS();
        Main$$CONSTS();
        Run$$CONSTS();
        Main$$CREATES();
        Run$$CREATES();
        Main$$FUNCTIONS();
        Run$$FUNCTIONS();
        Main$$SIZES();
        Run$$SIZES();
        Main$$TYPES();
        Run$$TYPES();
        if (isROV) {
            Main$$ROV();
            Run$$ROV();
        }//isROV
        $$SINGLETONS();
        Main$$SINGLETONS();
        Run$$SINGLETONS();
        $$INITIALIZATION();
    }
}
