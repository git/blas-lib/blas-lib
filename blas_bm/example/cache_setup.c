/******************************************************************************
 * (C) Copyright 2013, Texas Instruments Incorporated
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/
#include <stdio.h>
#include <time.h>
#include <c6x.h>

/*---------------------------------------------------------------------------*/
/* This program will initialize the C6678 configuration registers to utilize */
/* the L1 Cache, L2 Cache, and fix the MAR (Memory Attribute Registers) to   */
/* make all memory cacheable and pre-fetchable. Here is the layout of        */
/* control registers, specific to the C6678. They are memory-mapped          */
/* registers; write to the location and the internal register gets written.  */
/* All are 32-bit registers.                                                 */
/*                                                                           */
/* Address    Function                 Value                                 */
/* 0x01840000 Level 2 Cache Config     0x==== 0007 (all cache)               */
/* 0x01845004 L2 Write-back invalidate 0x0000 0001 (flushes L2 cache)        */
/* 0x01840040 Level 1 Cache Config     0x==== 0007 (all cache)               */
/* 0x01845044 L1 Write-back invalidate 0x0000 0001 (flushes L1 cache)        */
/* 0x01848000 16 reserved read-only MAR registers.                           */
/* 0x01848040 240 MAR regs.       |=   0x0000 0009 (8=prefetch, 1=cacheable) */
/* 0x018483FC Last of the MAR regs.                                          */
/*            For MAR regs, bits 1-2 are reserved, so we leave them as-is.   */
/* MAR reg 16 controls memory 1000 0000h to 10FF FFFFh,                      */
/* MAR reg 17 controls memory 1100 0000h to 11FF FFFFh, etc,                 */
/* MAR rg 255 controls memory FF00 0000h to FFFF FFFFh.                      */
/* Notice that MAR16 holds range of L1 SRAM, DO NOT cache that page!         */
/* L1 config 0--7 == disabled, 4K,8K,16K,32K,32K,32K, max (=32K)             */
/* L2 config 0--7 == disabled, 32K,64K,128K,256K,512K,1M,max (=1M).          */
/* We set our L2 to '5' because the link file, C6678_unified.cmd, maps our   */
/* L2 space to just 512K.                                                    */
/*                                                                           */
/* Interrupt stuff:                                                          */
/* 0x01800040 4 Event Clear Registers, to 0x0180004C.                        */
/* 0x01800080 4 Event Mask Registers, to 0x0180008C.                         */
/* 0x01800104 3 Interrupt Mux registers, to 0x0180010C.                      */
/* ISTP is the Interrupt Service Table pointer.                              */
/* ISTP & 0xFFFFC000 is the base address, +0x01E0 is INT15 ISR (8 words).    */
/* IER is an actual register (not to be confused with the EDMA channel       */
/*     control memory-mapped IER registers) that controls whether interrupts */
/*     4..15 are enabled.                                                    */
/*                                                                           */
/* Modified Oct 18: Set L1 to just 4K, so 28K could be used as fast ram.     */
/* Modified Mar  4: Set L2 to just 256K, so 256K could be used as fast ram.  */
/* Nov 29: Added code to report on mapping of EDMA3CC (DMA, QDMA transfers). */ 
/*---------------------------------------------------------------------------*/
int main(int argc, char **args)
{
   TSCL=0;  /* Always start real-time clock, just in case timing. */
   volatile unsigned int *L1PCFG = (unsigned int *) (0x01840020);
   volatile unsigned int *L1CFG  = (unsigned int *) (0x01840040);
   volatile unsigned int *L2CFG  = (unsigned int *) (0x01840000);
   volatile unsigned int *MAR    = (unsigned int *) (0x01848040);
   volatile unsigned int *DEVSTAT= (unsigned int *) (0x02620020);
// volatile unsigned int *EDMA3CC0 = (unsigned int*) (0x02700000ul);
// volatile unsigned int *CC0_QEER = (unsigned int*) (0x02701084ul);
// volatile unsigned int *CC0_EER  = (unsigned int*) (0x02701020ul);
// volatile unsigned int *EDMA3CC1 = (unsigned int*) (0x02720000ul);
// volatile unsigned int *EDMA3CC2 = (unsigned int*) (0x02740000ul);
// volatile unsigned int *EVTMASKR = (unsigned int*) (0x01800080ul);
// volatile unsigned int *INTMUXR  = (unsigned int*) (0x01800104ul);
// volatile unsigned int *INTSVCT  = NULL;
   unsigned int i, v;

   v = *L1PCFG;                                 /* Get value. */
   printf("L1PCFG on entry: %08X.\n", v);       /* Report it. */
   v &= 0xFFFFFFF8;                             /* Force final bits 000b. */
   v |= 0x00000007;                             /* Force final bits 111b. */
   *L1PCFG = v;                                 /* Configure L1P Cache. */

   v = *L1CFG;                                  /* Get value. */
   printf("L1CFG on entry: %08X.\n", v);        /* Report it. */
   v &= 0xFFFFFFF8;                             /* Force final bits 000b. */
   v |= 0x00000001;                             /* Force final bits 001b. */
                                                /*  (28K work, 4K cache). */
   *L1CFG = v;                                  /* Configure L1 Cache. */

   v = *L2CFG;                                  /* Get value. */
   printf("L2CFG on entry: %08X.\n", v);        /* Report it. */
   v &= 0xFFFFFFF8;                             /* Force final bits 000b. */
   v |= 0x00000004;                             /* Force final bits 100b. */
   *L2CFG = v;                                  /* Configure L2 Cache. */

   v = *MAR;                                    /* Get typical MAR value. */
   printf("MAR[16] on entry: %08X.\n", v);      /* Report it. */
   MAR[0] = 0;                                  /* range of L1D SRAM! */ 
   v = *MAR;                                    /* Read again. */
   printf("MAR[16] Modified: %08X.\n", v);      /* Report it. */
   v = 0x00000009;                              /* Value for MAR register. */
   for (i=1; i<240; i++)                        /* For all other MAR regs, */
      MAR[i] |= v;                              /* Configure address range.*/

   printf("Cache Setup Complete: L1CFG=%08X, L2CFG=%08X.\n", *L1CFG, *L2CFG);
   printf("DEVSTAT: %08X.\n", *DEVSTAT);
   fflush(stdout);
 
   /* Report on setup of interrupts. */
// for (i=0; i<4; i++) 
//    printf("EVTMASKR[%1i]=%08X.\n", i, EVTMASKR[i]);

// for (i=0; i<3; i++) 
//    printf("INTMUXR[%1i]=%08X.\n", i, INTMUXR[i]);

// v = IER;                                  // get the interrupt Enable ptr.       
// printf("IER=%08X.\n", v);                 // Report.

// v = ISTP;                                 // get the interrupt service table ptr.
// printf("ISTP=%08X.\n", v);                // Report.

// v &= 0xFFFFFC00;                          // Clear low part.
// v |= 0x000001E0;                          // Interrupt 15.
// INTSVCT = (unsigned int*) (v);            // Recast.
// for (i=0; i<8; i++)
//    printf("ISR_15[%1i]=%08X.\n", i, INTSVCT[i]);

   /* Report on setup of CC0. */
// printf("CC0 EER=%08X; CC0 QEER=%08X. (Event Enabled Registers).\n", 
//    *CC0_EER, *CC0_QEER);

   /* Report on the contents of QDMA 0-7 mapping, for each channel. */
   /* Offset is 0x200 = 512, but in 4-byte ints, 128.               */
// for (i=0; i<8; i++)
//    printf("EDMA3CC0 QDMA(%1i)=0x%08X.\n", i,EDMA3CC0[128+i]);
// for (i=0; i<8; i++)
//    printf("EDMA3CC1 QDMA(%1i)=0x%08X.\n", i,EDMA3CC1[128+i]);
// for (i=0; i<8; i++)
//    printf("EDMA3CC2 QDMA(%1i)=0x%08X.\n", i,EDMA3CC2[128+i]);

   return(0);
} /* END main */
