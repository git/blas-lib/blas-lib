export COMPILER_BASE_DIR="/opt/ti/TI_CGT_C6000_7.4.2"
export C6X_C_DIR="${COMPILER_BASE_DIR}/include;${COMPILER_BASE_DIR}/lib"
export PATH="${COMPILER_BASE_DIR}/bin:$PATH"
export XDCROOT="/opt/ti/xdctools/"
