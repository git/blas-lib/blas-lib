cblas Tests
===========

Edit sourceme.sh to suit your environment
$ source sourceme.sh
$ make clean
$ make all
$ make run


sgemm expected output:
 [ 367.76, 368.12
   674.06, 674.72 ]

matrix_vector expected output:
3.0  1.0  3.0
1.0  5.0  9.0
2.0  6.0  5.0
-1.0
 3.0
-3.0

SAMPLE_OUTPUT.txt contains a session output for reference.
