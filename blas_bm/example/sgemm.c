/*
*             Automatically Tuned Linear Algebra Software v3.11.0
*                    (C) Copyright 1997 R. Clint Whaley
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*   1. Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright
*      notice, this list of conditions, and the following disclaimer in the
*      documentation and/or other materials provided with the distribution.
*   3. The name of the ATLAS group or the names of its contributers may
*      not be used to endorse or promote products derived from this
*      software without specific written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
* TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
* PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE ATLAS GROUP OR ITS CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*/
#include <stdio.h>
#include "cblas.h"
     
int main (void)
{
       int lda = 3;
     
       float A[] = { 0.11, 0.12, 0.13,
                     0.21, 0.22, 0.23 };
     
       int ldb = 2;
       
       float B[] = { 1011, 1012,
                     1021, 1022,
                     1031, 1032 };
     
       int ldc = 2;
     
       float C[] = { 0.00, 0.00,
                     0.00, 0.00 };
     
       /* Compute C = A B */
     
       cblas_sgemm (CblasRowMajor, 
                    CblasNoTrans, CblasNoTrans, 2, 2, 3,
                    1.0, A, lda, B, ldb, 0.0, C, ldc);
     
       printf ("[ %g, %g\n", C[0], C[1]);
       printf ("  %g, %g ]\n", C[2], C[3]);
     
       return 0;  
}
